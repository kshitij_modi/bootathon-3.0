![logo.png](images/logo.png)
>- ## Computer Science & Engineering

 ### **<span style="color: #0099cc">Artificial Neural Networks</span>**
 * ### **<span style="color: #0099cc">INTRODUCTION</span>**
  The objective of this lab is to provide hands-on experience in understanding the basics of ANN models, and the pattern recognition tasks they perform. Some applications of ANN for problems in optimization and image processing will also be explored through these lab experiments.
  #### Important Notes:
  * If some or all of the tabs in this page or the experiment page are not visible, kindly try reloading or refreshing the page.
  * Some of the content uses MathJax for rendering equations. Rendering maybe slow on some systems. If the equations are not visible, you may have to refresh or reload the page.
  * Internet explorer is not supported in the current release. ANN Lab has been checked on Firefox and Opera.
### **<span style="color: #0099cc">EXPERIMENTS</span>**
1. [Parallel and distributed processing - I: Interactive activation and competition models](http://cse22-iiith.vlabs.ac.in/exp1/Objective.html?domain=Computer%20Science&lab=Artificial%20Neural%20Networks)

2. [Parallel and distributed processing - II: Constraint satisfaction neural network models](http://cse22-iiith.vlabs.ac.in/exp2/Objective.html?domain=Computer%20Science&lab=Artificial%20Neural%20Networks)
3. [Perceptron learning](http://cse22-iiith.vlabs.ac.in/exp3/Objective.html?domain=Computer%20Science&lab=Artificial%20Neural%20Networks)
4. [Multilayer feedforward neural networks](http://cse22-iiith.vlabs.ac.in/exp4/Objective.html?domain=Computer%20Science&lab=Artificial%20Neural%20Networks)
5. [Hopfield model for pattern storage task](http://cse22-iiith.vlabs.ac.in/exp5/Objective.html?domain=Computer%20Science&lab=Artificial%20Neural%20Networks)
6. [Hopfield model with stochastic update](http://cse22-iiith.vlabs.ac.in/exp6/Objective.html?domain=Computer%20Science&lab=Artificial%20Neural%20Networks)
7. [Competitive learning neural networks for pattern clustering](http://cse22-iiith.vlabs.ac.in/exp7/Objective.html?domain=Computer%20Science&lab=Artificial%20Neural%20Networks)
8. [Solution to travelling salesman problem using self organizing maps](http://cse22-iiith.vlabs.ac.in/exp8/Objective.html?domain=Computer%20Science&lab=Artificial%20Neural%20Networks)
9. [Solution to optimization problems using Hopfield models](http://cse22-iiith.vlabs.ac.in/exp9/Objective.html?domain=Computer%20Science&lab=Artificial%20Neural%20Networks)
10. [
Weighted matching problem: Deterministic, stochastic and mean-field annealing of an Hopfield model](http://cse22-iiith.vlabs.ac.in/exp10/Objective.html?domain=Computer%20Science&lab=Artificial%20Neural%20Networks)

### **<span style="color: #0099cc">TARGET AUDIENCE</span>**
* Senior undergraduate students
* Postgraduate students
* Research scholars
### **<span style="color: #0099cc">COURSE ALIGNED</span>**
This virtual lab on Artificial neural networks is to be used as a supplement to a parallel course on the same topic. It is not designed to be a complete course/tutorial by itself where a student can learn the concepts of neural network models just by going through the designed experiments.
### **<span style="color: #0099cc">PRE-REQUISTIES</span>**
#### Course requirements
  *  None
  #### Software compatibilty
  * Operating system: Linux, Windows 7, Mac OS 10.6
  * Browser: Firefox 3+, Safari, Opera, Chrome
  * Browser plugins: Sun java JRE and plugin Version-6
  
  [Click here for steps to install Java and Icedtea plugin](http://cse22-iiith.vlabs.ac.in/installJava.html "Installation step")
  #### Software Hardware requirements (minimum)
  * CPU: 1 GHz
  * Memory: 1 GB