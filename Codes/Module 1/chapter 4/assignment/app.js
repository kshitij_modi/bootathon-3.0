//variable having values of adding numbers
var num1 = document.getElementById("txt_box_1");
var num2 = document.getElementById("txt_box_2");
var addition = document.getElementById("txt_box_add_ans");
//variable having values of subtracting numbers
var num3 = document.getElementById("txt_box_3");
var num4 = document.getElementById("txt_box_4");
var subtract = document.getElementById("txt_box_sub_ans");
//variable having values of multiplying numbers
var num5 = document.getElementById("txt_box_5");
var num6 = document.getElementById("txt_box_6");
var multiply = document.getElementById("txt_box_mul_ans");
//variable having values of dividing numbers
var num7 = document.getElementById("txt_box_7");
var num8 = document.getElementById("txt_box_8");
var divide = document.getElementById("txt_box_div_ans");
//function for the addition of 2 variable
function fun_add() {
    if (isNaN(parseFloat(num1.value))) {
        alert("Enter only number");
    }
    else if (isNaN(parseFloat(num2.value))) {
        alert("Enter only number");
    }
    else {
        var add_ans = parseFloat(num1.value) + parseFloat(num2.value);
        addition.value = add_ans.toString();
    }
}
//function for the subtraction of 2 variable
function fun_sub() {
    if (isNaN(parseFloat(num3.value))) {
        alert("Enter only number");
    }
    else if (isNaN(parseFloat(num4.value))) {
        alert("Enter only number");
    }
    else {
        var sub_ans = parseFloat(num3.value) - parseFloat(num4.value);
        subtract.value = sub_ans.toString();
    }
}
//function for the multiplication of 2 variable
function fun_mul() {
    if (isNaN(parseFloat(num5.value))) {
        alert("Enter only number");
    }
    else if (isNaN(parseFloat(num6.value))) {
        alert("Enter only number");
    }
    else {
        var mul_ans = parseFloat(num5.value) * parseFloat(num6.value);
        multiply.value = mul_ans.toString();
    }
}
//function for the division of 2 variable
function fun_div() {
    var num = parseFloat(num8.value);
    if (isNaN(parseFloat(num7.value))) {
        alert("Enter only number");
    }
    else if (isNaN(parseFloat(num8.value))) {
        alert("Enter only number");
    }
    else if (num == 0) {
        alert("Number can not be divided by zero");
    }
    else {
        var div_ans = parseFloat(num7.value) / num;
        divide.value = div_ans.toString();
    }
}
//# sourceMappingURL=app.js.map