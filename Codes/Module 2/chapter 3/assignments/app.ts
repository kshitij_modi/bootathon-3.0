// function to check 
function check(){

    //get the value from the input elements.
    var t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1") ;
    var t2:HTMLInputElement = <HTMLInputElement>document.getElementById("t2") ;
    var t3:HTMLInputElement = <HTMLInputElement>document.getElementById("t3") ;
    var t4:HTMLInputElement = <HTMLInputElement>document.getElementById("t4") ;
    var t5:HTMLInputElement = <HTMLInputElement>document.getElementById("t5") ;
    var t6:HTMLInputElement = <HTMLInputElement>document.getElementById("t6") ;
    var t7:HTMLInputElement = <HTMLInputElement>document.getElementById("t7") ;
    var t8:HTMLInputElement = <HTMLInputElement>document.getElementById("t8") ;

    //convert all valuea into float.
    var x1 : number = parseFloat(t1.value);
    var y1 : number = parseFloat(t2.value);
    var x2 : number = parseFloat(t3.value);
    var y2 : number = parseFloat(t4.value);
    var x3 : number = parseFloat(t5.value);
    var y3 : number = parseFloat(t6.value);
    var x : number = parseFloat(t7.value);
    var y : number = parseFloat(t8.value);

    //check whether all the inputs are valid number or not.
    if(isNaN(x1 && y1 && x2 && y2 && x3 && y3 && x &&y))
    {
        alert("Please enter number only");
    }
    else{

        //calculate are of abc,abp,pbc,apc triangle.
        var a_abc:number =  Math.abs(( (x1*(y2-y3)) + (x2*(y3-y1)) + (x3*(y1-y2)) )/2);
        var a_abp:number =  Math.abs(( (x1*(y2-y)) + (x2*(y-y1)) + (x*(y1-y2)) )/2);
        var a_pbc:number =  Math.abs(( (x*(y2-y3)) + (x2*(y3-y)) + (x3*(y-y2)) )/2);
        var a_apc:number =  Math.abs(( (x1*(y-y3)) + (x*(y3-y1)) + (x3*(y1-y)) )/2);

        // sum of area of abp,pbc,apc triagngle.
        var sum:number = (a_abp + a_pbc + a_apc);
    
        //check if area of abc - sum is less then 0.000001 then point coordinates inside the triangle.
        if(Math.abs(a_abc - sum) < 0.000001){
            document.getElementById("message").innerHTML="Point coordinates inside the triangle";
        }
        else{
            document.getElementById("message").innerHTML="Point coordinates are not inside the triangle";
        }
    }
    
}
