// function to generate table
function generate(){
    var input:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    var num:number = parseInt(input.value);

    // check the entered value is number or character
    if(isNaN(num)){ 
        alert("Enter number only");
    }
    else{ 

        // created a table variable and provide style to table id = table_1 .
        var table:HTMLTableElement = <HTMLTableElement>document.getElementById("table_1");
        table.style.border = "1px solid black";
       
        //counter variable
        var count:number = 1;

        // created a tbl_header variable and assign value to it.
        var tbl_head : InnerHTML = <InnerHTML>document.getElementById("table_header");
        tbl_head.innerHTML="Table of "+ num.toString();
        
         // to delete the table row if exists
        while(table.rows.length>1){
            table.deleteRow(1);
        }
       
         // to create rows anc cell and append the data to cell
        for(count=1; count<=num; count++){

            //creating row.
            var row:HTMLTableRowElement = table.insertRow();

            //creating cell and provding style to it.
            var cell:HTMLTableDataCellElement = row.insertCell();
            cell.style.border="1px solid black";
            cell.style.textAlign="center";

            //creating paragraph tag and appending data to it
            var text = document.createElement("p");
            var ans=num*count;
            var result = document.createTextNode(num.toString()+" * "+count.toString()+ " = "+ans.toString());
            text.appendChild(result);

            // appending paragrapg tag data to the table cell.
            cell.appendChild(text);
        }
    }
}